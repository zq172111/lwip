#include "socket_wrap.h"

/**
 * @brief  创建套接字
 * @param  domain: 		协议族 选择IPV4或IPV6
 * @param  type: 		协议类型 选择TCP、UDP或RAW
 * @param  protocol: 	协议版本 一般为0
 * @retval 失败:		-1(申请创建套接字失败)
 *			成功:		非负整数(socket描述符可以链接到lwip_sock)
 */
int My_Socket(int domain, int type, int protocol)
{
	int fd; // socket的返回值
	fd = socket(domain, type, protocol);
	// 当返回值为-1，大概率是lwip的内存不够
	if (fd == -1)
	{
		puts("create socket error");
		vTaskDelete(NULL); // 删除自身进行任务调度
	}
	return fd;
}

/**
 * @brief  绑定套接字
 * @param  s: 			要绑定的socket套接字
 * @param  name: 		指向sockaddr的结构体指针（包含网卡IP、端口号等）
 * @param  namelen: 	name结构体长度
 * @retval 失败:		-1
 *			成功:		0
 */
int My_Bind(int s, const struct sockaddr *name, socklen_t namelen)
{
	int ret;
	ret = bind(s, name, namelen);
	// 绑定失败
	if (ret == -1)
	{
		puts("bind socket error");
		vTaskDelete(NULL); // 删除自身进行任务调度
	}
	return ret;
}

/**
 * @brief  在客户端中向服务器建立连接
 * @param  s: 			要连接的socket套接字
 * @param  name: 		指向sockaddr的结构体指针（包含网卡IP、端口号等）
 * @param  namelen: 	name结构体长度
 * @retval 失败:		-1
 *			成功:		0
 */
int My_Connect(int s, const struct sockaddr *name, socklen_t namelen)
{
	int ret;
	ret = lwip_connect(s, name, namelen);
	// 连接失败
	if (ret == -1)
	{
		puts("connect socket error");
		close(s); // 连接失败关闭当前的socket（内部会自动删除socket内存块）
	}
	return ret;
}

/**
 * @brief  监听套接字
 * @param  s: 			要监听的socket
 * @param  backlog: 	请求队列的大小
 * @retval 失败:		-1
 *			成功:		0
 */
int My_Listen(int s, int backlog)
{
	int ret;
	ret = listen(s, backlog);
	// 监听失败
	if (ret == -1)
	{
		puts("listen socket error");
		vTaskDelete(NULL); // 删除自身进行任务调度
	}
	return ret;
}

/**
 * @brief  等待客户端连接请求
 * @param  s: 			请求的socket
 * @param  addr: 		绑定的客户端地址、端口号等信息
 * @param  addrlen: 	地址结构体长度
 * @retval 失败:		-1
 *			成功:		非负整数(socket描述符可以链接到lwip_sock)
 */
int My_Accept(int s, struct sockaddr *addr, socklen_t *addrlen)
{
	int fd;
again_accept:
	// 阻塞函数，错误返回或者连接成功返回
	fd = accept(s, addr, addrlen);
	// 客户端连接错误
	if (fd == -1)
	{
		puts("accept connect error");
		goto again_accept; // 重新请求连接
	}
	return fd;
}

/**
 * @brief  向套接字发送数据
 * @param  s: 			发送数据到socket
 * @param  data: 		发送的缓冲区
 * @param  size: 		发送数据大小（单位字节）
 * @retval 失败:		-1
 *			成功:		返回一个新的套接字描述符（用于和已连接的客户端通信）
 */
int My_Write(int s, const void *data, size_t size)
{
	int ret;
	ret = write(s, data, size);
	// 发送失败（socket错误或者对方关闭连接）
	if (ret < 0)
	{
		puts("write data to socket error");
		close(s);
	}
	return ret;
}

/**
 * @brief  向套接字读取数据
 * @param  s: 			接收数据的socket
 * @param  data: 		接受的缓冲区
 * @param  size: 		接收数据大小（单位字节）
 * @retval 失败:		-1
 *		   成功:		返回已经接收的数据长度
 */
int My_Read(int s, void *mem, size_t len)
{
	int ret;
	ret = read(s, mem, len);
	// 读取失败（socket错误或者对方关闭连接）
	if (ret <= 0)
	{
		puts("read data to socket error");
		close(s);
	}
	return ret;
}

/**
 * @brief  发送数据到指定地址
 * @param  s: 			发送数据的套接字
 * @param  data: 		发送数据的起始地址
 * @param  size: 		数据长度
 * @param  flags: 		发送时的处理（默认为0）
 * @param  to: 			远端主机的IP和端口号
 * @param  tolen: 		信息长度
 * @retval 失败:		-1
 *		   成功:		返回已经发送的数据长度
 */
int My_Sendto(int s, const void *data, size_t size, int flags,
			  const struct sockaddr *to, socklen_t tolen)
{
	int ret;
again_sendto:
	ret = sendto(s, data, size, flags, to, tolen);
	if (ret == -1)
	{
		puts("sendto socket error");
		goto again_sendto;
	}
	return ret;
}

/**
 * @brief  接收数据到指定地址
 * @param  s: 			接收数据的套接字
 * @param  mem: 		接收数据的缓冲起始地址
 * @param  len: 		数据长度
 * @param  flags: 		接收时的处理（默认为0）
 * @param  from: 		远端主机的IP和端口号
 * @param  fromlen: 	信息长度
 * @retval 失败:		-1
 *		   成功:		返回已经接收的数据长度
 */
int My_Recvfrom(int s, void *mem, size_t len, int flags,
				struct sockaddr *from, socklen_t *fromlen)
{
	int ret;
again_recvfrom:
	ret = recvfrom(s, mem, len, flags, from, fromlen);
	if (ret == -1)
	{
		puts("sendto socket error");
		goto again_recvfrom;
	}
	return ret;
}
