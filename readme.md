# 谷子权谋

学习记录，代码就不怎么整理了

```
lwip_base_config	  	基础配置 lwip

lwip_tcp_pass			tcp回传实现（单片机是服务器，电脑是客户端）

lwip_tcp_server			tcp server端和tcp client端实现

lwip_tcp_socket			完善版tcp程序（封装好了）

lwip_tcp_socket_thread	并发服务器两种实现（多任务和多路IO）

lwip_dns				dns实现 路由192.168.1.1

lwip_dns_2.0			dns实现  路由192.168.0.1

lwip_tcp_heart			心跳机制实现

lwip_web				web点灯实现
```

