#ifndef __SOCKET_WRAP_H
#define __SOCKET_WRAP_H

#include "lwip/sockets.h"
#include "FreeRTOS.h"
#include "task.h"

int My_Socket(int domain, int type, int protocol);                     // 创建套接字
int My_Bind(int s, const struct sockaddr *name, socklen_t namelen);    // 绑定套接字
int My_Connect(int s, const struct sockaddr *name, socklen_t namelen); // 在客户端中向服务器建立连接
int My_Listen(int s, int backlog);                                     // 监听套接字
int My_Accept(int s, struct sockaddr *addr, socklen_t *addrlen);       // 等待客户端连接请求
int My_Write(int s, const void *data, size_t size);                    // 向套接字发送数据
int My_Read(int s, void *mem, size_t len);                             // 向套接字读取数据
int My_Sendto(int s, const void *data, size_t size, int flags,
              const struct sockaddr *to, socklen_t tolen); // 发送数据到指定地址
int My_Recvfrom(int s, void *mem, size_t len, int flags,
                struct sockaddr *from, socklen_t *fromlen); // 接收数据到指定地址
#endif
