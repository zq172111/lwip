#include "socket_dns_client.h"
#include "socket_tcp_server.h"
#include "lwip/sockets.h"
#include <ctype.h>
#include <netdb.h>

#define DNS_URL "www.baidu.com"
/* DNS客户端端（client）任务 */
void vDNSClient_Task(void)
{
    /* dns 域名解析功能 */
    int i;
    struct hostent *p_hostent = NULL;
    p_hostent = gethostbyname(DNS_URL);
    if (p_hostent)
    {
        for (i = 0; p_hostent->h_addr_list[i]; i++)
        {
            printf("url:%s\thost ip:%s\r\n", DNS_URL, inet_ntoa(*p_hostent->h_addr_list[i]));
        }
    }
    else
    {
        printf("get host ip fail!\r\n");
    }
	vTaskDelay(3000);
}
