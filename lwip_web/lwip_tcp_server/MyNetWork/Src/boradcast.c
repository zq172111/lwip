#include "boradcast.h" //广播
#include "socket_tcp_server.h"
#include <ctype.h>
#include "string.h"

//char UDPBoradcast_ReadBuff[BUFF_SIZE];

/* UDP任务广播 */
void vUDPBoradcast_Task(void)
{
    int sfd;
    int optval = 1;
    struct sockaddr_in client_addr; // 服务器和客户端的结构体
    socklen_t client_addr_len;
	puts("send..1...");
    // 创建socket
    sfd = My_Socket(AF_INET, SOCK_DGRAM, 0); // IPV4 通信方式 版本（默认0）
    setsockopt(sfd, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));
    client_addr.sin_family = AF_INET;
    client_addr.sin_port = htons(SERVER_PORT);
    client_addr.sin_addr.s_addr = inet_addr("255.255.255.255");
    client_addr_len = sizeof(client_addr);
	puts("send..2...");
    // 等待客户端连接
    while (1) // 等待客户端发送数据，然后进行大小写转换，再写回客户端
    {
		puts("send..3...");
        // 发送广播数据
        My_Sendto(sfd, "UDP", 10, 
			0, (struct sockaddr *)&client_addr, client_addr_len);
		puts("send.....");
        vTaskDelay(1000);
    }
}
