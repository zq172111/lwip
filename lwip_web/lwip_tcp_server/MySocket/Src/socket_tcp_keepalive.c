/***
 * #define setsockopt(s,level,optname,opval,optlen)  lwip_setsockopt(s,level,optname,opval,optlen)
 * int  lwip_setsockopt(int s, int level, int optname, const void *optval, socklen_t optlen)
 * s:       选择设置的套接字
 * level:   选项所在的协议层
 * optname: 需要访问的选项名
 * optval： 当前选项长度
 */

#include "socket_tcp_keepalive.h"
#include "socket_tcp_server.h"
#include "lwip/sockets.h"
#include <ctype.h>

static char ReadBuff[BUFF_SIZE];

/* TCP客户端端（client）任务 */
void vTCPClient_KeepAlive_Task(void)
{
    int cfd;
    int read_len;                   // 读的数据长度
    int write_len;                  // 写的数据长度
    struct sockaddr_in server_addr; // 服务器结构体
    int ret;                        // 连接状态
    int so_keepalive_data = 1;      // 保持连接秒数
    int tcp_keeppidle_data = 3;     // 发送心跳空闲周期
    int tcp_keepintvl_data = 3;     // 发送心跳间隔
    int tcp_keepcnt_data = 3;       // 心跳重发次数
    int tcp_dodelay = 1;            // nagle算法时间
again:
    // 创建socket
    cfd = My_Socket(AF_INET, SOCK_STREAM, 0); // IPV4 socker连接 默认协议
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = inet_addr(SERVER_IP); // 本机IPV4
    // 使能socket层的心跳检测
    setsockopt(cfd, SOL_SOCKET, SO_KEEPALIVE, &so_keepalive_data, sizeof(int)); // 选择客户端 socket层 保持连接 缓冲（保持连接秒数1秒） 长度4字节
    // 连接到服务器
    ret = My_Connect(cfd, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 文件描述符 ip地址和端口号 大小
    if (ret == -1)
    {
        vTaskDelay(100); // 100ms连接一次服务器
        goto again;
    }
    // 配置心跳连接参数(因为默认参数时间很长)
    setsockopt(cfd, IPPROTO_TCP, TCP_KEEPIDLE, &tcp_keeppidle_data, sizeof(int));
    setsockopt(cfd, IPPROTO_TCP, TCP_KEEPINTVL, &tcp_keepintvl_data, sizeof(int));
    setsockopt(cfd, IPPROTO_TCP, TCP_KEEPCNT, &tcp_keepcnt_data, sizeof(int));
    setsockopt(cfd, IPPROTO_TCP, TCP_NODELAY, &tcp_dodelay, sizeof(int));

    puts("server is connect ok");
    printf("server is connect cfd = %d\r\n", cfd);
    while (1)
    {
        read_len = My_Read(cfd, ReadBuff, BUFF_SIZE); // 等待服务器发送数据
        if (read_len == -1)
        {
            goto again;
        }
        for (int i = 0; i < read_len; i++)
        {
            ReadBuff[i] = toupper(ReadBuff[i]); // 小写转换为大写
        }
        write_len = My_Write(cfd, ReadBuff, read_len); // 再写回服务器
        if (write_len == -1)
        {
            goto again;
        }
    }
}
