#include "socket_thread_server.h"
#include "socket_tcp_server.h"

//读写数组
char RW_Clent_Addr[BUFF_SIZE];

/* 创建新的客户端任务 */
void vNewClient_Task(void const *argument)
{
    int cfd_temp = *(int *)argument; //客户端的套接字(文件描述符)
    int read_len, write_len; //读写的长度
    // 调试打印
    printf("client number = %d\r\n", cfd_temp);
    while (1)
    {
        //读取客户端发送的数据
        read_len = My_Read(cfd_temp, RW_Clent_Addr, BUFF_SIZE);
        if (read_len == -1)
        {
            close(cfd_temp);
            vTaskDelete(NULL);
        }
        //进行大小写转换
        for (int i = 0; i < read_len; i++)
        {
            RW_Clent_Addr[i] = toupper(RW_Clent_Addr[i]);
        }
        //将数据写回客户端
        write_len = My_Write(cfd_temp, RW_Clent_Addr, read_len);
        if (write_len == -1)
        {
            close(cfd_temp);
            vTaskDelete(NULL);
        }
        vTaskDelay(10);
    }
}

/* TCP服务端任务（多线程） */
void vTCPServer_ThreadTask(void)
{
    int sfd;                                     // 服务端返回值
    int cfd;                                     // 客户端返回值
    struct sockaddr_in server_addr, client_addr; // 服务端和客户端地址结构体
    socklen_t client_addr_len;
    // 创建socket
    sfd = My_Socket(AF_INET, SOCK_STREAM, 0); // IPV4 TCP
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);       // 大小端转换
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // IP前后转换
    // 绑定socket
    My_Bind(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    // 监听socket
    My_Listen(sfd, 5);
    // 等待客户端连接
	client_addr_len = sizeof(client_addr);
	while (1)
	{
		cfd = My_Accept(sfd, (struct sockaddr *)&client_addr, &client_addr_len);
		// 打印调试
		printf("client number = %d\r\n", cfd);

		// 任务创建
		if (xTaskCreate((TaskFunction_t)vNewClient_Task, "newclient", 256, (void *)&cfd, 1, NULL) != pdPASS)
		{
			puts("create new client task fail");
		}
	}
}
