#ifndef __SOCKET_THREAD_SERVER_H
#define __SOCKET_THREAD_SERVER_H

void vNewClient_Task(void const * argument); //创建客户端任务
void vTCPServer_ThreadTask(void); //TCP服务端任务（多线程）

#endif
