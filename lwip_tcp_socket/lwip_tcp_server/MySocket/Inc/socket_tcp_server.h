#ifndef __SOCKET_TCP_SERVER_H
#define __SOCKET_TCP_SERVER_H

#include "socket_wrap.h"

#define SERVER_PORT 5555 // 端口号
#define BUFF_SIZE 1024
/* 单片机作为客户端需要连接服务器，这是服务器的IP（socker_tcp_client被调用） */
#define SERVER_IP "192.168.1.10"	
void vTCPServer_Task(void);

#endif
