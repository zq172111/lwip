#include "socket_dns_client.h"
#include "socket_tcp_server.h"
#include "lwip/sockets.h"
#include <ctype.h>
#include <netdb.h>
#include "lwip/dns.h"
#include "lwip/ip_addr.h"

// 先声明回调函数
err_t dns_found(const char *name, ip_addr_t *ipaddr, void *callback_arg);

/* DNS客户端端（client）任务 */
void vDNSClient_Task(void)
{
    /* dns 域名解析功能 */
    int i;
    struct hostent *p_hostent = NULL;
    p_hostent = gethostbyname("www.baidu.com");
    if (p_hostent)
    {
        for (i = 0; p_hostent->h_addr_list[i]; i++)
        {
            printf("host ip:%s\r\n", inet_ntoa(*(struct in_addr *)p_hostent->h_addr_list[i]));
        }
    }
    else
    {
        printf("get host ip fail!\r\n");
    }
}

// 定义回调函数
err_t dns_found(const char *name, ip_addr_t *ipaddr, void *callback_arg) {
    if (ipaddr != NULL) {
        printf("Host: %s found at IP: %s\n", name, ipaddr_ntoa(ipaddr));
    } else {
        printf("Host: %s not found.\n", name);
    }
    return ERR_OK;
}

void resolve_hostname(void) {
    ip_addr_t ipaddr;
    err_t err = dns_gethostbyname("www.baidu.com", &ipaddr, (dns_found_callback)dns_found, NULL);
    if (err == ERR_OK) {
        printf("DNS query for %s already cached: IP is %s\n", "www.baidu.com", ipaddr_ntoa(&ipaddr));
    } else if (err == ERR_INPROGRESS) {
        printf("DNS query for %s sent. Waiting for callback...\n", "www.baidu.com");
    } else {
        printf("DNS query failed with error: %d\n", err);
    }
}
