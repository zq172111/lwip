#include "socket_udp_server.h"
#include "socket_tcp_server.h"
#include <ctype.h>

char UDPServer_ReadBuff[BUFF_SIZE];

/* UDP服务器端（server）任务 */
void vUDPServer_Task(void)
{ 
    int sfd;
    int read_len;                                // 读的数据长度
    struct sockaddr_in server_addr, client_addr; // 服务器和客户端的结构体
    socklen_t client_addr_len;
    // 创建socket
    sfd = My_Socket(AF_INET, SOCK_DGRAM, 0); // IPV4 通信方式 版本（默认0）
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // 从本机获取
    // 绑定socket
    My_Bind(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 文件描述符 IP地址加端口号 addr长度 返回值

    client_addr_len = sizeof(client_addr);
    // 等待客户端连接
    while (1) // 等待客户端发送数据，然后进行大小写转换，再写回客户端
    {
        read_len = My_Recvfrom(sfd, UDPServer_ReadBuff, BUFF_SIZE, 0, (struct sockaddr *)&client_addr, &client_addr_len);
        for (int i = 0; i < read_len; i++)
        {
            UDPServer_ReadBuff[i] = toupper(UDPServer_ReadBuff[i]); // 小写转换为大写
        }
        My_Sendto(sfd, UDPServer_ReadBuff, BUFF_SIZE, 0, (struct sockaddr *)&client_addr, client_addr_len);
    }
}
