#include "socket_tcp_client.h"
#include "socket_tcp_server.h"
#include "lwip/sockets.h"
#include <ctype.h>

static char ReadBuff[BUFF_SIZE];

/* TCP客户端端（client）任务 */
void vTCPClient_Task(void)
{
    int cfd;
    int read_len;                   // 读的数据长度
    struct sockaddr_in server_addr; // 服务器结构体
    // 创建socket
    cfd = socket(AF_INET, SOCK_STREAM, 0); // IPV4 socker连接 默认协议
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = inet_addr(SERVER_IP); // 本机IPV4
	
    // 连接到服务器
    connect(cfd, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 文件描述符 ip地址和端口号 大小
    printf("server is connect ok");
    printf("server is connect cfd = %d\r\n", cfd);
    while (1)
    {
        read_len = read(cfd, ReadBuff, BUFF_SIZE); // 等待服务器发送数据
        for (int i = 0; i < read_len; i++)
        {
            ReadBuff[i] = toupper(ReadBuff[i]); // 小写转换为大写
        }
        write(cfd, ReadBuff, read_len); // 再写回服务器
    }
}
