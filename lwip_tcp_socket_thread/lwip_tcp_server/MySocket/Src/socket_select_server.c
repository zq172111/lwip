/***
 * fd_set的常用宏
 * FD_ZERO(&set):初始化fd_set,将所有为清零
 * FD_SET(fd, &set):将文件描述符fd添加到fd_set中
 * FD_CLR(fd, &set):从fd_set中移除文件描述符fd
 * FD_ISSET(fd, &set):检查文件描述符fd是否在fd_set中
 */

#include "socket_select_server.h"
#include "socket_tcp_server.h"
#include "string.h"

static char Client_Addr[BUFF_SIZE]; // 客户端数据数组

/* 并发服务器任务 */
void vSelectServer_Task(void)
{
    int sfd, cfd;                                // 服务端和客户端返回值
    int maxfd, readys;                           // 最大文件描述符和结构体中准备好的套接字数量
    int read_clent_len, write_clent_len;         // 读、写取客户端的数据长度
    struct sockaddr_in server_addr, client_addr; // 服务端和客户端地址结构体
    socklen_t client_addr_len;                   // 客户端结构体长度
    fd_set all_set, read_set;                    // fd_set是一组跟踪套接字事件(是否可读、可写)的数据结构
    int Client_Fds[FD_SETSIZE - 1];              // fd_size里面包含了服务器的fd
    // 创建socket
    sfd = My_Socket(AF_INET, SOCK_STREAM, 0); // IPV4 socker连接 默认协议
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SERVER_PORT);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // 从本机获取
    // 绑定socket
    My_Bind(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr)); // 文件描述符 IP地址加端口号 addr长度 返回值
    // 监听socket
    My_Listen(sfd, 5); // 文件描述符 握手队列总和
    // 初始化
    maxfd = sfd;
    FD_ZERO(&all_set);     // 初始化all_set将所有位清零
    FD_SET(sfd, &all_set); // 服务器文件描述符添加到集合中
    for (int i = 0; i < FD_SETSIZE - 1; i++)
    {
        Client_Fds[i] = -1;
    }
    while (1)
    {
        read_set = all_set;                                      // 每次select时,fd_set集合就会变化,所以要保存设置fd_set和读取的fd_set
        readys = select(maxfd + 1, &read_set, NULL, NULL, NULL); // select函数来监测一个或多个套接字事件
        if (readys < 0)                                          // select会死等，会在上行代码卡住，如果其小于0即程序运行错误
        {
            puts("select error");
            vTaskDelete(NULL);
        }
        // 判断监听的套接字是否有数据
        if (FD_ISSET(sfd, &read_set))
        {
            cfd = My_Accept(sfd, (struct sockaddr *)&client_addr, &client_addr_len);
            if (cfd < 0)
            {
                puts("client accept error");
                continue; // 继续select
            }
            // 打印调试
            printf("client number = %d\r\n", cfd);
            // 将新的cfd添加到fd_set集合中然后更新
            FD_SET(cfd, &all_set);
            maxfd = (cfd > maxfd) ? cfd : maxfd;
            for (int i = 0; i < FD_SETSIZE - 1; i++)
            {
                if (Client_Fds[i] == -1)
                {
                    Client_Fds[i] = cfd;
                    break;
                }
            }
            if (--readys == 0)
            {
                continue;
            }
        }
        // 遍历所有的客户端文件描述符
        for (int i = 0; i < FD_SETSIZE - 1; i++)
        {
            if (Client_Fds[i] == -1)
            {
                continue;
            }
            // 判断是否在fd_set集合中
            if (FD_ISSET(Client_Fds[i], &read_set))
            {
                read_clent_len = My_Read(Client_Fds[i], Client_Addr, BUFF_SIZE);
                if (read_clent_len <= 0)
                {
                    FD_CLR(Client_Fds[i], &all_set);
                    Client_Fds[i] = -1;
                }
                // 进行大小写转换
                for (int i = 0; i < read_clent_len; i++)
                {
                    Client_Addr[i] = toupper(Client_Addr[i]);
                }
                // 写回客户端
                write_clent_len = My_Write(Client_Fds[i], Client_Addr, read_clent_len);
                if (write_clent_len <= 0)
                {
                    FD_CLR(Client_Fds[i], &all_set);
                    Client_Fds[i] = -1;
                }
            }
        }
    }
}
